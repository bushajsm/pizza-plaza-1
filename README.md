
Requirements
PHP 7.3+ or above  
Instructions
1-composer install
2-npm install
3-copy example.env to .env file and provide the database credentials
4-php artisan migrate
5-php artisan db:seed
6-php artisan serve
7-npm run dev
Admin credentials
email : admin@gmail.com
password : admin1234
User credentials or you can directly create one
email : user@gmail.com
password : user1234
